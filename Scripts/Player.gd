class_name Player
extends "res://Scripts/Character.gd"

func _physics_process(delta):
	#input_strength = Vector2(
	#	Input.get_action_strength("right") - Input.get_action_strength("left"),
	#	Input.get_action_strength("down") - Input.get_action_strength("up")
	#)
	_tick(delta)

func _unhandled_input(event):
	if event.is_action_pressed("left_click") && astar_grid:
		var global_mouse_position = get_local_mouse_position() + global_position
		go_to_pos(global_mouse_position)
		
	if event.is_action_pressed("show_wings"):
		$Skin/Wings.set("visible", !$Skin/Wings.get("visible"))
		if $Skin/Wings.get("visible"):
			playback_speed *= 2
			speed *= 2
		else:
			playback_speed /= 2
			speed /= 2
