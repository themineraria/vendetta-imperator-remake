class_name Character
extends CharacterBody2D

signal hit

@export var tile_map: TileMap
@export var skin_texture: Texture2D
@onready var animation_tree = $AnimationTree
@onready var animation_state_machine: AnimationNodeStateMachinePlayback = animation_tree.get("parameters/playback")
@export var speed: float = 50 # How fast the player will move (pixels/sec).
@export var playback_speed: float = 1 # How fast the player the play animations will move.
@onready var current_position = position
@onready var current_tile_data = null
@onready var current_tile_speed = null
#@export var swim_crop: float = 0.65 # How much to crop the player when swiming
var screen_size # Size of the game window.
var astar_grid: AStarGrid2D
var input_strength = Vector2(0,0)
#var current_id_path: Array[Vector2i]
var astar_current_point_path: PackedVector2Array # Used to draw the path line of the player
var crop_y_ratio: float = 1 # Used by the shader to crop the player

# Called when the node enters the scene tree for the first time.
func _ready():
	if skin_texture == null:
		print("NO SKIN ASSIGNED TO CHARACTER")
	$Skin.texture = skin_texture
	#Make sure each instance of character has its own unique material
	material = material.duplicate()
	screen_size = get_viewport_rect().size
	if tile_map:
		astar_grid = AStarGrid2D.new()
		astar_grid.region = tile_map.get_used_rect()
		astar_grid.cell_size = Vector2(16,16)
		astar_grid.diagonal_mode = AStarGrid2D.DIAGONAL_MODE_ONLY_IF_NO_OBSTACLES
		astar_grid.update()
		set_astar_weights_and_solids()
		current_tile_data = tile_map.get_cell_tile_data(0, tile_map.local_to_map(position))
		current_tile_speed = current_tile_data.get_custom_data("walk_speed") if current_tile_data else 1.
	else:
		print("NO TILE MAP ASSIGNED TO CHARACTER")
	animation_tree.active = true

func set_astar_weights_and_solids():
	for x in range(tile_map.get_used_rect().size.x):
		for y in range(tile_map.get_used_rect().size.y):
			var tile_position = Vector2i(
				x + tile_map.get_used_rect().position.x,
				y + tile_map.get_used_rect().position.y
			)
			
			for layer in range(tile_map.get_layers_count()):
				var tile_data = tile_map.get_cell_tile_data(layer, tile_position)
				var tile_speed = tile_data.get_custom_data("walk_speed") if tile_data else 1
				astar_grid.set_point_weight_scale(tile_position, 1./tile_speed)
				
				if tile_speed == 0:
					astar_grid.set_point_solid(tile_position)

func update_shader_parameters():
	# Get the player sprite current frame information
	var spriteTexture : Texture = $Skin.texture
	var textureSize : Vector2 = spriteTexture.get_size()
	var frameSize : Vector2 = Vector2(textureSize.x / $Skin.hframes, textureSize.y / $Skin.vframes)
	var currentFramePosition : Vector2 = Vector2(frameSize.x * $Skin.frame_coords.x, frameSize.y * $Skin.frame_coords.y)

	var start_y: float = currentFramePosition.y / textureSize.y
	var end_y: float = (currentFramePosition.y+frameSize.y) / textureSize.y
	
	material.set_shader_parameter("start_y", start_y);
	# Crop the bottom of the current frame
	material.set_shader_parameter("end_y", start_y+(end_y-start_y)*crop_y_ratio);

func update_animation(direction: Vector2 = Vector2(0,0), tile_speed: float = 1):
	if direction != Vector2(0,0):
		animation_tree.set("parameters/Walk/Input/blend_position", direction)
		animation_tree.set("parameters/Walk/TimeScale/scale", playback_speed * tile_speed)
		animation_state_machine.travel("Walk", false)
	elif animation_state_machine.get_current_node() != "Idle":
		animation_tree.set("parameters/Walk/Input/blend_position", direction)
		animation_tree.set("parameters/Walk/TimeScale/scale", playback_speed * tile_speed)
		animation_state_machine.travel("Idle", false)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	_tick(delta)

func _tick(delta):
	move(delta)
	#if is_on_wall():
	#	print("hit")
	#	hit.emit()
		# disabling the player's collision so that we don't trigger the hit signal more than once.
		# $CollisionShape2D.set_deferred("disabled", true)

var lt = 0.0
func move(delta):
	if !astar_current_point_path.is_empty(): # A* algorithm for auto path finding
		var target_position = astar_current_point_path[0]
		lt += (0.05 * current_tile_speed)
		position = current_position.lerp(target_position, clamp(lt, 0, 1))
		update_animation(current_position.direction_to(target_position), current_tile_speed)
		if position.distance_to(target_position) <= 8:
			current_tile_data = tile_map.get_cell_tile_data(0, tile_map.local_to_map(position)) if tile_map else null
			current_tile_speed = current_tile_data.get_custom_data("walk_speed") if current_tile_data else 1.
			crop_y_ratio = current_tile_data.get_custom_data("crop_y_ratio") if current_tile_data and current_tile_data.get_custom_data("crop_y_ratio") else 1.
		if position == target_position:
			lt = 0
			current_position = position
			astar_current_point_path = astar_current_point_path.slice(1)
	else:
		update_animation(Vector2(0,0), current_tile_speed)

func go_to_pos(pos):
	astar_current_point_path.clear()
	lt = 0
	current_position = position
	astar_current_point_path = astar_grid.get_point_path(
			tile_map.local_to_map(global_position),
			tile_map.local_to_map(pos)
		).slice(1)
	for i in astar_current_point_path.size():
		astar_current_point_path[i] = astar_current_point_path[i] + (astar_grid.cell_size/2)

func _on_sprite_2d_frame_changed():
	#The frame has changed, we need to update the shader parameters (e.g. position of the crop)
	update_shader_parameters()

func _on_area_2d_area_entered(area):
	if area is Ressource:
		$Inventory.harvest_resource(area)

func _on_area_2d_area_exited(area):
	if area is Ressource:
		$Inventory.stop_harvest_resource()
