class_name Map
extends TileMap

var resource_scene = preload("res://Scenes/ressource.tscn")
var pixel_to_resource_map = {}
# Called when the node enters the scene tree for the first time.
func _init():
	init_resource_map()
	print(pixel_to_resource_map)
	
	var map = Image.load_from_file(GameState.map_folder_path+"Terrain.bmp")
	var ressource_map = Image.load_from_file(GameState.map_folder_path+"Ressources.bmp")
	var left_border = (0-map.data["width"])/2
	var top_border = (0-map.data["height"])/2
	var map_data: PackedByteArray = map.data["data"]
	var ressource_map_data: PackedByteArray = ressource_map.data["data"]
	#print("size: " + str(map_data.size()/4))
	assert(map.data["width"] == ressource_map.data["width"])
	assert(map.data["height"] == ressource_map.data["height"])
	assert(map.data["width"] >= 100)
	assert(map.data["height"] >= 75)
	var grass = []
	var dirt = []
	var sand = []
	var mountain = []
	var snow = []
	var cobble = []
	var water = []
	for x in range(int(map_data.size()/4)):
		var map_pixel = Vector4i(map_data[(x+1)*4-4], map_data[(x+1)*4-3], map_data[(x+1)*4-2], map_data[(x+1)*4-1])
		var ressource_map_pixel = Vector4i(ressource_map_data[(x+1)*4-4], ressource_map_data[(x+1)*4-3], ressource_map_data[(x+1)*4-2], ressource_map_data[(x+1)*4-1])
		var row_idx: int = x/map.data["width"]
		var col_idx: int = x - row_idx * map.data["width"]
		var pixel_pos = Vector2i(left_border+col_idx, top_border+row_idx)
		
		#Set default terrain as grass
		set_cell(0, pixel_pos, 0, Vector2i(0, 0), 0)
		
		match map_pixel:
			Vector4i(49, 156, 99, 255): grass.append(pixel_pos)
			Vector4i(132, 132, 0, 255): dirt.append(pixel_pos)
			Vector4i(255, 255, 0, 255): sand.append(pixel_pos)
			Vector4i(148, 148, 148, 255): mountain.append(pixel_pos)
			Vector4i(255, 255, 255, 255): snow.append(pixel_pos)
			Vector4i(198, 198, 198, 255): cobble.append(pixel_pos)
			Vector4i(0, 206, 255, 255): water.append(pixel_pos)
			Vector4i(255, 206, 0, 255): set_cell(0, pixel_pos, 0, Vector2i(0, 7), 0) #Wall
		
		match ressource_map_pixel:
			Vector4i(0, 0, 0, 255): pass
			_: create_ressource(ressource_map_pixel, pixel_pos)

	set_cells_terrain_connect(0, grass, 0, 1)
	set_cells_terrain_connect(0, dirt, 0, 2)
	set_cells_terrain_connect(0, sand, 0, 3)
	set_cells_terrain_connect(0, mountain, 0, 4)
	set_cells_terrain_connect(0, snow, 0, 5)
	set_cells_terrain_connect(0, cobble, 0, 6)
	set_cells_terrain_connect(0, water, 0, 7)
	#set_cells_terrain_connect(0, wall, 1, 3) #Doesn't work, not sure why, maybe 'cause its another terrain set ?
	#pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func create_ressource(pixel: Vector4i, pos: Vector2i):
	if pixel_to_resource_map.has(pixel):
		var name = pixel_to_resource_map[pixel]
		var ressource = Ressource.create(StaticData.resources[name])
		ressource.position = (pos*16)+Vector2i(8,0)
		add_child(ressource)

func init_resource_map():
	for res in StaticData.resources.keys():
		var pixel = Vector4i(StaticData.resources[res]["color_key"][0], StaticData.resources[res]["color_key"][1], StaticData.resources[res]["color_key"][2], 255)
		pixel_to_resource_map[pixel] = StaticData.resources[res]["name"]
