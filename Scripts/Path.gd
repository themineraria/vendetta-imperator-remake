extends Node2D

@export var player: CharacterBody2D

func _process(_delta):
	queue_redraw()

func _draw():
	var current_point_path: PackedVector2Array = player.astar_current_point_path;
	if current_point_path.is_empty() or current_point_path.size() < 2:
		return
	
	draw_polyline(current_point_path, Color.from_hsv(0,0,255,0.5))
