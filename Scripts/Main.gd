extends Node2D

@onready var player_camera = $"Player/Camera"
@onready var free_camera = $Map/Camera
var character_scene = preload("res://Scenes/character.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	player_camera.make_current()
	for j in range(12):
		for x in range(90):
			var bot1 = create_bot("res://Images/Persos/NainsF/Nain-F1.bmp", -98*8 + x*16, -70*8 + j*16)
			bot1.go_to_pos(Vector2(-98*8 + x*16, 70*8))

func _input(event):
	if event.is_action_pressed("switch_camera"):
		if player_camera.is_current():
			free_camera.position = player_camera.global_position
			free_camera.make_current()
		else:
			player_camera.make_current()
	elif event.is_action_pressed("quit"):
		get_tree().change_scene_to_file("res://Scenes/menu.tscn")

func create_bot(skin, x, y) -> Node:
	var character_instance = character_scene.instantiate()
	character_instance.skin_texture = load(skin)
	character_instance.tile_map = $Map
	character_instance.position.x = x
	character_instance.position.y = y
	add_child(character_instance)
	return character_instance
