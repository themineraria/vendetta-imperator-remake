class_name ZoomingCamera2D
extends Camera2D

enum CameraType {
	Free,
	Following,
}

@export var tilemap: TileMap
@export var type: CameraType = CameraType.Free

const free_margin = 50
const free_speed = 450 
const max_zoom = 2.0
var min_zoom = 1
# Controls how much we increase or decrease the `zoom` on every turn of the scroll wheel.
const zoom_factor = 0.1

# View port size & position
@onready var visible_region = get_viewport().get_visible_rect()

# Size of the world in tiles
@onready var tilemap_size = tilemap.get_used_rect()
# Size of a tile in px
@onready var tile_x_size = tilemap.tile_set.tile_size.x
@onready var tile_y_size = tilemap.tile_set.tile_size.y

# Position of each world border in px
@onready var world_left_border = tilemap_size.position.x * tile_x_size
@onready var world_top_border = tilemap_size.position.y * tile_y_size
@onready var world_right_border = (tilemap_size.position.x + (tilemap_size.size.x-1)) * tile_x_size
@onready var world_bottom_border = (tilemap_size.position.y + (tilemap_size.size.y-1)) * tile_y_size

func _zoom(value: Vector2):
	zoom += value
	zoom = zoom.clamp(Vector2(min_zoom, min_zoom), Vector2(max_zoom, max_zoom))

func _unhandled_input(event):
	if event.is_action_pressed("zoom_in"):
		_zoom(Vector2(zoom_factor, zoom_factor))
	if event.is_action_pressed("zoom_out"):
		_zoom(Vector2(-zoom_factor, -zoom_factor))

func _ready():	
	# Set the position for the camera with a margin of one tile in pixels (world border)
	limit_left = world_left_border + tile_x_size
	limit_top = world_top_border + tile_y_size
	limit_right = world_right_border
	limit_bottom = world_bottom_border
	
	# Calculate the zoom ratio between the tilemap size and the viewport, on the X and Y axis
	var x_zoom_ratio = snappedf(visible_region.size.x/(limit_right-limit_left), 0.01)
	var y_zoom_ratio = snappedf(visible_region.size.y/(limit_bottom-limit_top), 0.01)
	
	# Set the maximum zoom ratio, as the minimum zoom possible
	min_zoom = clamp(x_zoom_ratio if x_zoom_ratio > y_zoom_ratio else y_zoom_ratio, 1, 2)
	
	#print(min_zoom)

func _physics_process(delta):
	if type == CameraType.Free and is_current():
		var mouse_position = get_global_mouse_position()
		
		# Position of each camera border
		var left_border = position.x - (visible_region.size.x/zoom.x)/2
		var top_border = position.y - (visible_region.size.y/zoom.y)/2
		var right_border = position.x + (visible_region.size.x/zoom.x)/2
		var bottom_border = position.y + (visible_region.size.y/zoom.y)/2
		
		# Distance bewteen each camera border and the center of the camera
		var left_and_right_border_to_center_distance = (visible_region.size.x/zoom.x)/2
		var top_and_bottom_border_to_center_distance = (visible_region.size.y/zoom.y)/2
		
		#print("left: " + str(left_border) + " right: " + str(right_border) + " mouse: " + str(mouse_position))
		var movement = Vector2(0,0)
		
		#If we are in the range between the left border and the margin
		if left_border <= mouse_position.x and mouse_position.x <= left_border + free_margin:
			movement.x -= free_speed * delta
		#If we are in the range between the right border and the margin
		if right_border-free_margin <= mouse_position.x and mouse_position.x <= right_border:
			movement.x += free_speed * delta
		#If we are in the range between the top border and the margin
		if top_border <= mouse_position.y and mouse_position.y <= top_border + free_margin:
			movement.y -= free_speed * delta
		#If we are in the range between the bottom border and the margin
		if bottom_border-free_margin <= mouse_position.y and mouse_position.y <= bottom_border:
			movement.y += free_speed * delta
		
		#print("top: " + str(top_border) + " mouse global: " + str(mouse_position) + " mouse local: " + str(DisplayServer.mouse_get_position()))
		
		# Do the movement
		position += movement
		
		# Limit the camera position when zooming out or if too far away
		position.x = clamp(position.x, world_left_border+left_and_right_border_to_center_distance, world_right_border-left_and_right_border_to_center_distance)
		position.y = clamp(position.y, world_top_border+top_and_bottom_border_to_center_distance, world_bottom_border-top_and_bottom_border_to_center_distance)
