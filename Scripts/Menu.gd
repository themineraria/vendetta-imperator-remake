extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	GameState.map_folder_path = "res://Cartes/(02) Duel/"
	pass

func _on_button_pressed():
	#get_tree().change_scene_to_file("res://Scenes/main.tscn")
	get_tree().change_scene_to_file("res://Scenes/load.tscn")

func _on_button_2_pressed():
	print("WIP")

func _on_nb_peuple_changed(value):
	GameState.nb_peuple = value

func _on_map_list_item_selected(index):
	match index:
		0: 
			GameState.map_folder_path = "res://Cartes/(02) Duel/"
			GameState.nb_peuple = 2
		1: 
			GameState.map_folder_path = "res://Cartes/(06) Dunes/"
			GameState.nb_peuple = 6
