extends CanvasLayer

var _scene_path = "res://Scenes/main.tscn"
var _progress = []
var _loaded_resource = PackedScene.new()
@onready var _progress_bar = $Panel/TextureProgressBar

func _ready():
	start_load()

func start_load():
	var state = ResourceLoader.load_threaded_request(_scene_path, "PackedScene", false)
	if state == OK:
		set_process(true)

func _process(_delta):
	var load_status = ResourceLoader.load_threaded_get_status(_scene_path, _progress)
	match load_status:
		0,2: #? THREAD_LOAD_INVALID_RESOURCE, THREAD_LOAD_FAILED
			set_process(false)
			print("load error")
			return
		1: #? THREAD_LOAD_IN_PROGRESS
			update_progress_bar(_progress[0])
		3: #? THREAD_LOAD_LOADED
			_loaded_resource = ResourceLoader.load_threaded_get(_scene_path)
			#var scene = _loaded_resource.instantiate()
			update_progress_bar(1.0)
			get_tree().change_scene_to_packed(_loaded_resource)

func update_progress_bar(new_value: float):
	_progress_bar.set_value_no_signal(new_value*100)
