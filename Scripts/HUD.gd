extends CanvasLayer

var inventory: Inventory
@onready var resources_list = $Resources/ScrollContainer/GridContainer
var chroma_key_shader: Shader = preload("res://Shaders/chroma-key.gdshader")
var resource_values_list = {}
func _ready():
	await get_tree().get_root().get_node("/root/main").ready
	inventory = get_tree().get_root().get_node("/root/main/Player/Inventory")
	for ressource_name in inventory.resources:
		resource_values_list[ressource_name] = create_ressource_row(ressource_name)
	
func _input(event):
	if event.is_action_pressed("show_fps"):
		$"FPS meter".set("visible", !$"FPS meter".get("visible"))
	
	if event.is_action_pressed("show_inventory"):
		$Inventory.set("visible", !$Inventory.get("visible"))
	
	if event.is_action_pressed("show_resources"):
		$Resources.set("visible", !$Resources.get("visible"))

func _on_resources_gui_input(event: InputEvent):
	if event.is_action_pressed("left_click"):
		get_tree().get_root().set_input_as_handled()
	elif event.is_action_pressed("zoom_in") || event.is_action_pressed("zoom_out"):
		get_tree().get_root().set_input_as_handled()

func refresh(window, key):
	print("Window: " + str(window) + " what: " + str(key))
	match window:
		"resource": resource_values_list[key].text = str(inventory.resources[key])
		_: pass

func create_ressource_row(ressource_name) -> Label:
	var icon = TextureRect.new()
	icon.texture = load(StaticData.resources[ressource_name]["icon"])
	var material = ShaderMaterial.new()
	material.shader = chroma_key_shader.duplicate()
	material.set_shader_parameter("u_replacement_color", Color(0, 0, 0, 0))
	material.set_shader_parameter("tolerance", 0.05)
	icon.material = material
	var name = Label.new()
	name.text = ressource_name + ": "
	var value = Label.new()
	value.text = str(inventory.resources[ressource_name])
	resources_list.add_child(icon)
	resources_list.add_child(name)
	resources_list.add_child(value)
	return value
