class_name Inventory
extends Node

var timed_target_buf
var resources: Dictionary

func _init():
	for name in StaticData.resources.keys():
		resources[name] = 0
	
func harvest_resource(ressource: Ressource):
	if resources[ressource.get_resource_name()] < ressource.get_max_amount():
		timed_target_buf = ressource
		$HarvestResourceTimer.start(1)

func stop_harvest_resource():
	$HarvestResourceTimer.stop()
	timed_target_buf = null

func _on_harvest_resource_timer_timeout():
	if timed_target_buf is Ressource:
		var name = timed_target_buf.take_one()
		resources[name] += 1
		print("Harvested: " + name)
		get_tree().call_group("hud", "refresh", "resource", name)
