class_name Ressource
extends Area2D

const scene: PackedScene = preload("res://Scenes/ressource.tscn")

var data: Dictionary
var amount = 20

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_resource_name():
	return data["name"]

func get_max_amount():
	return data["max_amount"]

static func create(d: Dictionary) -> Ressource:
	var ressource: Ressource = scene.instantiate()
	ressource.data = d
	ressource.get_child(0).texture = load(d["sprite"])
	return ressource

func take_one() -> String:
	amount -= 1
	if amount == 0:
		queue_free()
	return data["name"]
