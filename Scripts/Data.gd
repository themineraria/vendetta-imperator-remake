class_name Data
extends Node

#This script loads all the data for the game to run 
# (i.e. all the const values for ressources, items, characters, etc.)

#Used to validate json objs, like a json schema
var json_schema = {
	"resource": {
		"name": {"type": TYPE_STRING},
		"sprite": {"type": TYPE_STRING, "validator": [Callable(FileAccess, 'file_exists'), Callable(self, 'is_valid_resource_image')]},
		"color_key": {"type": TYPE_ARRAY, "validator": [Callable(self, 'is_rgb_color')]},
		"max_amount": {"type": TYPE_FLOAT},
		"icon": {"type": TYPE_STRING, "validator": [Callable(FileAccess, 'file_exists'), Callable(self, 'is_valid_resource_image')]},
	}
}

var resources: Dictionary = {}

func _init():
	get_jsons()
	print(resources)

func get_jsons(path = "res://Donnees"):
	var dir = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var obj_name = dir.get_next()
		while obj_name != "":
			if dir.current_is_dir():
				print_debug("Found data directory: " + obj_name)
				get_jsons(path + "/" + obj_name)
			elif obj_name.ends_with(".json"):
				print_debug("Found json: " + obj_name)
				load_json(path+"/"+obj_name)
			obj_name = dir.get_next()
	else:
		print_debug("An error occurred when trying to access the data path.")

func load_json(path):
	var file = FileAccess.open(path, FileAccess.READ)
	var content = file.get_as_text()
	var json = JSON.new()
	var error = json.parse(content)
	if error == OK:
		var json_data = json.data
		if is_json_valid(json_data):
			import_json(json_data)
	else:
		print("JSON Parse Error: ", json.get_error_message(), " in ", path, " at line ", json.get_error_line())

func is_json_valid(json_data: Dictionary) -> bool:
	if !json_data.has("type"):
		return false
	match json_data["type"]:
		"bundle":
			if !json_data.has("content") || typeof(json_data["content"]) != TYPE_ARRAY:
				return false
			for obj in json_data["content"]:
				if !is_json_valid(obj):
					return false
				import_json(obj)
		_ :
			if !json_schema.has(json_data["type"]):
				return false
			var validator: Dictionary = json_schema[json_data["type"]]
			if !json_data.has_all(validator.keys()):
				return false
			for key in validator.keys():
				if typeof(json_data[key]) != validator[key]["type"]:
					return false
				if validator[key].has("validator"):
					for v in validator[key]["validator"]:
						if !v.call(json_data[key]):
							return false
	return true

func is_rgb_color(values: Array) -> bool:
	if values.size() != 3:
		return false
	for v in values:
		if v < 0 || v > 255:
			return false
	return true

func is_valid_resource_image(path: String) -> bool:
	if !path.ends_with(".bmp"):
		return false
	var img = Image.load_from_file(path)
	if img.data["width"] != 32 || img.data["height"] != 32:
		return false
	return true

func import_json(obj: Dictionary):
	match obj["type"]:
		"bundle":
			for o in obj["content"]:
				import_json(o)
		"resource":
			resources[obj["name"]] = obj
